There are four folders in this Repository:


Analysis: This folder includes the Raw data, R Scripts used to analyze the data and results of the analysis

Dataset: datasets and visualizations used in this study


Experiment: Web-based software we used for our experiment


Posters: Sample of the posters we used in the first phase of the experiment. 